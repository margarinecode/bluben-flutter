import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decode/jwt_decode.dart';

class ProfileService{
  Future<Map<String, dynamic>> getProfileData ()  async {
    final storage = new FlutterSecureStorage();
    String token = await storage.read(key: 'token') as String;
    final data = Jwt.parseJwt(token!);
    return data;
  }
}

