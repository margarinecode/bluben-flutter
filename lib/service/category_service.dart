import 'dart:convert';
import 'dart:developer';

import 'package:bluben/pages/login/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:bluben/model/category.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:http/http.dart' as http;

class ApiService {
  Dio _dio() {
    final options = BaseOptions(
      connectTimeout: 300,
      receiveTimeout: 300,
      contentType: "application/json;charset=utf-8",
    );
    var dio = Dio(options);

    if (foundation.kDebugMode) {
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          compact: true
      ));
    } else {}

    return dio;
  }

  Future<List<CategoryModel>?> getUsers() async {
    try {
      final storage = new FlutterSecureStorage();
      final token =await  storage.read(key: 'token');
      print(token);
      var response = await _dio().get(
          'http://172.17.0.204:3222/category/list',
          options: Options(
            headers: {
              "Authorization": " Bearer $token"
            },
          )
      );


      if (response.statusCode == 200) {
        List _model = response.data["data"]["result"];
        // print(_model) ;
        return _model.map((e) => CategoryModel.fromJson(e)).toList();
      }else if(response.statusCode == 401){
        return [];
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
// }
