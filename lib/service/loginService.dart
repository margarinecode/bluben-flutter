import 'dart:collection';
import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:bluben/model/category.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:http/http.dart' as http;

class loginService {
  Dio _dio() {
    final options = BaseOptions(
      connectTimeout: 300,
      receiveTimeout: 300,
      contentType: "application/json;charset=utf-8",
    );
    var dio = Dio(options);

    if (foundation.kDebugMode) {
      dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          compact: true,
        error: true,
      ));
    } else {}

    return dio;
  }

  Future<String> updateAvatar(XFile file) async {
    final storage = new FlutterSecureStorage();
    String token = await storage.read(key: 'token') as String;
    final data = Jwt.parseJwt(token!);
    var response = await _dio().post(
        'http://172.17.0.204:3222/users/update/'+ data?['id'],
      data: {
          file : file
      },
    );
    if(response.statusCode == 200){
      return "Succesfully!";
    }else{
      return response.data['message'];
    }
  }

  Future<String> login(String email, String password) async {
      var response = await _dio().post(
        'http://172.17.0.204:3222/users/login',
        data: {
          'email': email,
          'password': password
        },);
      if(response.statusCode == 200){
        final storage = new FlutterSecureStorage();
        await storage.write(key: 'token', value: response.data['data']['token']);
        // result.addEntries();
        return response.data['message'];
      }else{
        return response.data['message'];
      }


  }

  Future<String> signup(String name , String email, String password) async {
    var response = await _dio().post(
      'http://172.17.0.204:3222/users/create',
      data: {
        'name': name,
        'email': email,
        'password': password,
        'photo': "test.hpg",
        'role':'79cfe9ea-25a8-46e5-b3d4-fba5d12c775d'
      },);
    // print(await response.data + "message");
    if(response.statusCode == 200){
      var res = await _dio().post(
        'http://172.17.0.204:3222/users/login',
        data: {
          'email': email,
          'password': password
        },);
      final storage = new FlutterSecureStorage();
      await storage.write(key: 'token', value:await  res.data['data']['token']);
      return response.data['message'];
    }else{
      return response.data['message'];
    }
  }

}
// }
