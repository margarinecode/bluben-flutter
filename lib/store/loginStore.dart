
import 'package:bluben/service/loginService.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';

class LoginStore = _LoginStore with Store;

abstract class _LoginStore with Store {
  // @observable
  String? email = '', password;


  loginService service = loginService();


  @action
  addAkun(String email, String password) async {
    this.email = email;
    this.password = password;
  }
}
