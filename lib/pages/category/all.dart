// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, must_be_immutable, use_key_in_widget_constructors, sized_box_for_whitespace, avoid_unnecessary_containers, non_constant_identifier_names

import 'package:bluben/pages/category/app_bar.dart';
import 'package:bluben/pages/category/custom_tab.dart';
import 'package:flutter/material.dart';

// class AllCate extends StatelessWidget {

// }

class AllCate extends StatefulWidget {
  const AllCate({super.key});

  @override
  State<AllCate> createState() => _AllCateState();
}

class _AllCateState extends State<AllCate> {
  bool woman = true;
  bool man = false;
  final title = 'Categories';
  final icon = Icon(
    Icons.shopping_bag,
    color: Colors.black,
    size: 25,
  );
  // List<String> items = ['Woman', 'Man'];
  // int current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 243, 243, 243),
      appBar: AppBarTop(
        title: title,
        icon: icon,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          children: [
            CustomBar(),
          ],
        ),
      ),
    );
  }
}
