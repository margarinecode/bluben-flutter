// ignore_for_file: prefer_const_constructors, non_constant_identifier_names

import 'package:bluben/model/category.dart';
import 'package:bluben/pages/category/list_woman.dart';
import 'package:bluben/service/category_service.dart';
import 'package:flutter/material.dart';

class CustomBar extends StatefulWidget {
  const CustomBar({super.key});

  @override
  State<CustomBar> createState() => _CustomBarState();
}

class _CustomBarState extends State<CustomBar> {
  late List<CategoryModel>? _categoryModel = [];
  @override
  void initState() {
    super.initState();
    _getData();
  }

  void _getData() async {
    _categoryModel = (await ApiService().getUsers())!;
    Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
  }
  List<String> items = ['Woman', 'Man'];
  int current = 0;
  List<String> cat_woman = [
    'Clothing',
    'Accessories',
    'Bags',
    'Footwear',
    'Sports'
  ];
  List<String> icons_woman = [
    'assets/icons/dress.svg',
    'assets/icons/glasses.svg',
    'assets/icons/handbag.svg',
    'assets/icons/heels.svg',
    'assets/icons/sport.svg',
  ];
  List<String> icons_man = [
    'assets/icons/clothing_man.svg',
    'assets/icons/watch.svg',
    'assets/icons/backpack.svg',
    'assets/icons/shoes.svg',
    'assets/icons/jersey.svg',
  ];
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          SizedBox(
            height: 60,
            child:_categoryModel == null || _categoryModel!.isEmpty
                ? const Center(
              child: CircularProgressIndicator(),
            )
                : ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              itemBuilder: (ctx, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      current = index;
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.all(5),
                    height: 50,
                    width: 150,
                    decoration: BoxDecoration(
                      border: current == index
                          ? Border.all(style: BorderStyle.solid)
                          : Border.all(style: BorderStyle.none),
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.transparent,
                    ),
                    child: Center(
                      child:
                      Text(
                        // items[index],
                        _categoryModel![index].name,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: 600,
            child:
            ListView.builder(
                // physics: BouncingScrollPhysics(),
                itemCount: _categoryModel?.length,
                itemBuilder: (context, index) {
                  return current == 0
                      ? ListWoman(
                          title: _categoryModel![index].name,
                          icon: icons_woman[index],
                        )
                      : ListWoman(
                          title: cat_woman[index],
                          icon: icons_man[index],
                        );
                }),
          ),
        ],
      ),
    );
  }
}
