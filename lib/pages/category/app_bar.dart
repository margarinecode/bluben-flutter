// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, override_on_non_overriding_member, use_key_in_widget_constructors, sort_child_properties_last, must_be_immutable

import 'package:flutter/material.dart';

class AppBarTop extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Icon icon;
  const AppBarTop({this.title = '', required this.icon});
  // DefaultTabController myController = DefaultTabController(length: 2, child: null)

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Color.fromARGB(255, 243, 243, 243),
      shadowColor: Colors.transparent,
      leading: BackButton(color: Colors.black),
      title: Text(
        title,
        style: TextStyle(
          color: Colors.black,
          fontSize: 25,
        ),
      ),
      actions: [icon],
      centerTitle: true,
    );
  }
}
