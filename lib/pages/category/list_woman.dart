// ignore_for_file: prefer_const_constructors, must_be_immutable, use_key_in_widget_constructors

import 'package:bluben/pages/category/detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ListWoman extends StatelessWidget {
  final String title;
  final String icon;
  const ListWoman({required this.title, required this.icon});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return DetailCat(
                title: title,
              );
            },
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
        height: 120,
        width: 100,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          children: [
            Container(
              height: 80,
              width: 80,
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              decoration: BoxDecoration(
                color: Color.fromARGB(
                  255,
                  243,
                  243,
                  243,
                ),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: SvgPicture.asset(
                  icon,
                  height: 40,
                  width: 40,
                ),
              ),
            ),
            SizedBox(
              width: 120,
              child: Text(
                title,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
              child: Icon(
                Icons.keyboard_arrow_right_outlined,
                size: 30,
                color: Colors.grey[500],
              ),
            )
          ],
        ),
      ),
    );
  }
}
