// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';

import 'app_bar.dart';
import '../home_app/product.dart';

class DetailCat extends StatefulWidget {
  final String title;
  const DetailCat({required this.title});

  @override
  State<DetailCat> createState() => _DetailCatState();
}

class _DetailCatState extends State<DetailCat> {
  // final title = 'Clothing';
  final icon = Icon(
    Icons.filter_list_alt,
    color: Colors.black,
    size: 25,
  );
  List<String> name = [
    'White shirt',
    'Stylish pantsuit',
    'test',
    'test2',
    'test3',
  ];
  List<String> img = [
    'assets/tshirt1.jpg',
    'assets/tshirt2.jpg',
    'assets/tshirt2.jpg',
    'assets/tshirt1.jpg',
    'assets/tshirt2.jpg',
  ];
  List<double> price = [
    72.00,
    74.00,
    80.00,
    90.00,
    60.00,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 243, 243, 243),
      appBar: AppBarTop(
        title: widget.title,
        icon: icon,
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 5,
          childAspectRatio: 0.6,
        ),
        physics: BouncingScrollPhysics(),
        itemCount: name.length,
        padding: EdgeInsets.only(left: 20),
        itemBuilder: (context, index) {
          return ProductHome(
            name: name[index],
            img: img[index],
            price: price[index],
            idx: index,
          );
        },
      ),
    );
  }
}
