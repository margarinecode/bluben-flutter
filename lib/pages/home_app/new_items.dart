// ignore_for_file: sized_box_for_whitespace, must_be_immutable, prefer_const_constructors

import 'package:bluben/pages/home_app/product.dart';
import 'package:flutter/material.dart';

class NewItem extends StatelessWidget {
  List<String> name = ['White shirt', 'Stylish pantsuit', 'White shirt', 'Stylish pantsuit'];
  List<String> img = ['assets/tshirt1.jpg', 'assets/tshirt2.jpg', 'assets/tshirt1.jpg', 'assets/tshirt2.jpg'];
  List<double> price = [72.00, 74.00, 72.00, 74.00];
  NewItem({super.key});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 270,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: name.length,
        itemBuilder: (context, index) {
          return ProductHome(
            name: name[index],
            img: img[index],
            price: price[index],
            idx: 0,
          );
        },
      ),
    );
  }
}
