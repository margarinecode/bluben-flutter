// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Promo extends StatelessWidget {
  const Promo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      width: 200,
      margin: EdgeInsets.fromLTRB(0, 0, 20, 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color.fromARGB(255, 243, 243, 243),
            ),
            child: Center(
              child: SvgPicture.asset(
                'assets/icons/disc.svg',
                width: 30,
              ),
            ),
          ),
          Column(
            children: [
              Container(
                width: 155,
                padding: const EdgeInsets.fromLTRB(10, 18, 0, 0),
                child: Text(
                  '50% OFF',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              Container(
                width: 155,
                padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Text(
                  "on all women's shoes",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.all(10),
            // color: Colors.amber,
            child: Icon(
              Icons.keyboard_arrow_right_outlined,
              size: 25,
              color: Colors.grey[500],
            ),
          )
        ],
      ),
    );
  }
}
