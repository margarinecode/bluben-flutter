// ignore_for_file: prefer_const_constructors

import 'package:bluben/pages/constants.dart';
import 'package:bluben/pages/home_app/index.dart';
import 'package:bluben/pages/login/index.dart';
import 'package:bluben/pages/profile/index.dart';
import 'package:flutter/material.dart';

class BottomApp extends StatefulWidget {
    final int active;
   BottomApp({required this.active});
  @override
  State<BottomApp> createState() => _BottomAppState();
}

class _BottomAppState extends State<BottomApp> {

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topRight: Radius.circular(40),
        topLeft: Radius.circular(40),
      ),
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: SecondColor,
        ),
        child: BottomNavigationBar(
          items:  [
            BottomNavigationBarItem(
                icon: Icon(Icons.home_filled),
                label: '.',
                tooltip: String.fromEnvironment('home')),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: '.',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_bag_rounded),
              label: '.',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: '.',
            ),
          ],
          unselectedItemColor: Colors.grey,
          selectedItemColor: Color.fromARGB(255, 156, 175, 136),
          currentIndex: widget.active,
          iconSize: 30,
          onTap: (index){
            switch (index){
              case(0): {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return HomePage();
                    },
                  ),
                );
              }
              break;
              case(1): {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return HomePage();
                    },
                  ),
                );
              }
              break;
              case(2): {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return HomePage();
                    },
                  ),
                );
              }
              break;
              case(3): {

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return Profile();
                    },
                  ),
                );
              }
            }
          },
          mouseCursor: MaterialStateMouseCursor.clickable,
          showSelectedLabels: false,
        ),
      ),
    );
  }
}

