// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, prefer_const_constructors_in_immutables, sized_box_for_whitespace, prefer_interpolation_to_compose_strings, prefer_adjacent_string_concatenation, unnecessary_brace_in_string_interps

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductHome extends StatelessWidget {
  final String name;
  final String img;
  final double price;
  final int idx;
  ProductHome({
    required this.name,
    required this.img,
    required this.price,
    required this.idx,
  });

  f() {
    if (idx % 2 == 0) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: f()
          ? EdgeInsets.fromLTRB(0, 20, 15, 0)
          : EdgeInsets.fromLTRB(0, 40, 15, 0),
      height: 200,
      width: 155,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: Image.asset(
              img,
              width: 155,
            ),
          ),
          Container(
            width: 155,
            padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
            child: Text(
              name,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 18,
                color: Colors.grey[500],
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            width: 155,
            child: Row(
              children: [
                Container(
                  width: 130,
                  child: Text(
                    '\$ ' + '${price}',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ),
                SvgPicture.asset(
                  'assets/icons/plus.svg',
                  width: 15,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
