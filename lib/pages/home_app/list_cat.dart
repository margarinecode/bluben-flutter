// ignore_for_file: sized_box_for_whitespace, prefer_const_constructors

import 'dart:ffi';

import 'package:bluben/model/category.dart';
import 'package:bluben/pages/home_app/circle.dart';
import 'package:bluben/service/category_service.dart';
import 'package:bluben/store/loginStore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ListCate extends StatefulWidget {
  const ListCate({Key? key}) : super(key: key);

  @override
  State<ListCate> createState() => _ListCateState();
}

class _ListCateState extends State<ListCate> {
  late List<CategoryModel>? _categoryModel = [];
  @override
  void initState() {
    super.initState();
    _getData();
  }

  void _getData() async {
    _categoryModel = (await ApiService().getUsers())!;
    Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
  }
  final List _top = [
    {
      'icon': SvgPicture.asset(
        'assets/icons/all.svg',
        width: 22,
      ),
      'text': "All"
    },
    {
      'icon': SvgPicture.asset(
        'assets/icons/dress.svg',
        width: 22,
      ),
      'text': "Clothing"
    },
    {
      'icon': SvgPicture.asset(
        'assets/icons/glasses.svg',
        width: 25,
      ),
      'text': "Accesorries"
    },
    {
      'icon': SvgPicture.asset(
        'assets/icons/handbag.svg',
        width: 22,
      ),
      'text': "Bag"
    }
  ];
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        height: 120,
        child:_categoryModel == null || _categoryModel!.isEmpty
            ? const Center(
              child: CircularProgressIndicator(),
        )
            : Observer(
            builder: (context)  => (ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: 4,
              itemBuilder: (context, index) {
                final String child;
                if(index == 0){
                  child = 'All';
                }else{
                  child = _categoryModel![index-1].name;
                }
                return CircleTop(child: child, icon: _top[index]['icon'],);

              },
            ))),
      ),
    );
  }
}

