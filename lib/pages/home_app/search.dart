// ignore_for_file: prefer_const_constructors

import 'package:bluben/pages/constants.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
      color: Colors.transparent,
      child: Row(
        children: [
          SizedBox(
            height: 50,
            width: 280,
            child: TextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(16),
                    borderSide:
                        BorderSide(color: PrimaryColor)),
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                hintText: 'Search',
                suffixIcon: Icon(Icons.search, color: Colors.black),
              ),
              cursorColor: PrimaryColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
            child: Icon(
              Icons.filter_list_alt,
              size: 32,
            ),
          ),
        ],
      ),
    );
  }
}
