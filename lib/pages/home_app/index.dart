// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_field, sized_box_for_whitespace
import 'package:bluben/pages/home_app/bottom_app_bar.dart';
import 'package:bluben/pages/home_app/list_cat.dart';
import 'package:bluben/pages/home_app/new_items.dart';
import 'package:bluben/pages/home_app/promo.dart';
import 'package:bluben/pages/home_app/search.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({super.key});
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // final List _post = ["All", "Cloting", "Accessories", "Bag"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomApp(active: 0,),
      backgroundColor: Color.fromARGB(255, 243, 243, 243),
      body: Padding(
        padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: ListView(
                children: [
                  SearchBar(),
                  ListCate(),
                  Promo(),
                  Text(
                    'New items',
                    style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  NewItem(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
