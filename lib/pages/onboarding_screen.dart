// ignore_for_file: library_private_types_in_public_api, prefer_const_constructors, prefer_final_fields, prefer_const_literals_to_create_immutables, unused_import

import 'package:bluben/pages/intro_screens/intro_page_1.dart';
import 'package:bluben/pages/intro_screens/intro_page_2.dart';
import 'package:bluben/pages/login/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'home_app/index.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  PageController _controller = PageController();

  bool onLastPage = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        PageView(
          controller: _controller,
          onPageChanged: (index) {
            setState(() {
              onLastPage = (index == 1);
            });
          },
          children: [
            IntroPage1(),
            IntroPage2(),
          ],
        ),
        Container(
          alignment: Alignment(0, 0.9),
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SmoothPageIndicator(
                controller: _controller,
                count: 2,
                effect: ExpandingDotsEffect(
                  dotColor: Colors.grey,
                  activeDotColor: Colors.black,
                  dotHeight: 3,
                  dotWidth: 70,
                ),
              ),
            ],
          ),
        ),
        Container(
            alignment: Alignment.bottomCenter,
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () async {
                    final storage = new FlutterSecureStorage();
                    final token = await storage.read(key: 'token');
                    print(await token.toString());
                    // _controller.jumpToPage(1);
                  },
                  child: Container(
                    alignment: Alignment(0, 0),
                    margin: const EdgeInsets.all(8.0),
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Skip',
                        style: TextStyle(
                          color: Colors.grey[500],
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          fontFamily: 'Nunito',
                        ),
                      ),
                    ),
                  ),
                ),
                onLastPage
                    ? GestureDetector(
                        onTap: () async {
                          final storage = new FlutterSecureStorage();
                          final token = await  storage.read(key: 'token');
                          print(token);
                          if(token == null || token == ''){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {

                                  return Login();
                                },
                              ),
                            );
                          }else{
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return HomePage();
                                },
                              ),
                            );
                          }

                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.black),
                          height: 50,
                          // alignment: Alignment(100, 100),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Text(
                              'Done',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Nunito',
                              ),
                            ),
                          ),
                        ),
                      )
                    : GestureDetector(
                        onTap: () {
                          _controller.nextPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeIn,
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.black),
                          height: 50,
                          // alignment: Alignment(100, 100),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Text(
                              'Next',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Nunito',
                              ),
                            ),
                          ),
                        ),
                      ),
              ],
            ))
      ],
    ));
  }
}
