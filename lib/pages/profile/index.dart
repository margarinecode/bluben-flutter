import 'dart:io';

import 'package:bluben/pages/constants.dart';
import 'package:bluben/pages/home_app/bottom_app_bar.dart';
import 'package:bluben/pages/home_app/circle.dart';
import 'package:bluben/pages/login/index.dart';
import 'package:bluben/service/loginService.dart';
import 'package:bluben/service/profile_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jwt_decode/jwt_decode.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late Map<String, dynamic>? _profileData = {};
  late XFile _img;
  late String img = _profileData?['id'];
  Future getImage(ImageSource media) async {
    var imgs = await ImagePicker().pickImage(source: media);
    var response = await loginService().updateAvatar(imgs!);
    print(response);
    setState(() {
      _img = imgs as XFile;
    });
  }
  final List data = [
    {
      'icon': Icon(Icons.add_shopping_cart_outlined),
      'title': "Order History"
    },
    {
      'icon': Icon(Icons.notifications_active_outlined),
      'title': "Notification"
    },
    {
      'icon': Icon(Icons.settings),
      'title': "Settings"
    },

    {
      'icon': Icon(Icons.info_outline),
      'title': "About"
    },
    {
      'icon': Icon(Icons.help),
      'title': "help"
    },
    {
      'icon': Icon(Icons.logout_outlined),
      'title': "Logout"
    },
  ];
  
  void myAlert() {
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        title: Text('Please choose image'),
        content:  Container(
          height: MediaQuery.of(context).size.height / 6,
          child: Column(
            children: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    getImage(ImageSource.gallery);
                  },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: SecondColor,
                      shadowColor: Colors.transparent,
                      fixedSize: Size.fromWidth(450),
                      textStyle: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontFamily: 'Nunito-bold'
                      )
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.image),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text("From Gallery"),
                      )
                    ],
                  )
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    getImage(ImageSource.camera);
                  },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: SecondColor,
                      shadowColor: Colors.transparent,
                      fixedSize: Size.fromWidth(450),
                      textStyle: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontFamily: 'Nunito-bold'
                      )
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.camera_alt_outlined),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text("From Camera"),
                      )
                    ],
                  )
              )
            ],
          ),
        ),
      );
    });
  }
  @override
  void initState() {
    super.initState();
    _getData();
  }

  void _getData() async {
    _profileData = (await ProfileService().getProfileData())!;
    Future.delayed(const Duration(seconds: 1)).then((value) => setState(() {}));
  }



  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomApp(active: 3,),
      body: Container(
        decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.1, 0.5],
                colors: [PrimaryColor, Colors.white10]
              )
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 80),
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        height: 120,
                        color: SecondColor,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ElevatedButton(
                                child: const Text('Edit Profile'),
                                onPressed: () => Navigator.pop(context),
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: SecondColor,
                                  shadowColor: Colors.transparent,
                                  fixedSize: Size.fromWidth(450),
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Nunito-bold'
                                  )
                                ),
                              ),
                              ElevatedButton(
                                child: const Text('Edit Avatar'),
                                onPressed: () => myAlert(),
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: SecondColor,
                                    shadowColor: Colors.transparent,
                                    fixedSize: Size.fromWidth(450),
                                    textStyle: TextStyle(
                                        fontSize: 16,
                                        fontFamily: 'Nunito-bold'
                                    )
                                ),
                              ),
                            ],
                          ),
                      );
                    },
                  );
                },
                child: Container(
                  height: 100,
                  width: 320,
                  margin: EdgeInsets.only(bottom: 50),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: _profileData == null || _profileData!.isEmpty ?
                  Center(
                    child: CircularProgressIndicator(),
                  )
                      :Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.network(
                         "http://172.17.0.204:3222/users/get/image/"+ _profileData?['id']  ,
                          height: 150,
                          width: 100,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 10),
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                          Text(
                              _profileData?['name'],
                            style: TextStyle(
                              fontFamily: "Nunito-bold",
                              fontWeight: FontWeight.w700,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Text(
                            _profileData?['sub'],
                            style: TextStyle(
                              fontFamily: "Nunito",
                              fontSize: 12,
                              height: 1.5,
                              leadingDistribution: TextLeadingDistribution.proportional
                            ),
                          //
                          //
                          ),
                          Row(
                            children: [
                              Icon(
                                  Icons.account_balance_wallet,
                                color: PrimaryColor,
                                size: 25,
                              ),
                              Text("Rp. 200,000")
                            ],
                          ),
                      ],
                    ),
                      ),

                    ],
                  ),
                ),
              ),
              Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: (){
                          print(index);
                          if(index == 5){
                            showDialog(context: context, builder: (context) => AlertDialog(
                              title: const Text('Log Out'),
                              content: const Text('Are you sure want to log out?'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () => Navigator.pop(context, 'Cancel'),
                                  child: const Text('Cancel'),
                                ),
                                TextButton(
                                  onPressed: (){
                                    var storage = new FlutterSecureStorage();
                                    storage.delete(key: "token");
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) {
                                          return Login();
                                        },
                                      ),
                                    );
                                  },
                                  child: const Text('OK'),
                                ),
                              ],
                            ));

                          }
                        },
                        child: Container(
                                  height: 60,
                                  width: 320,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      data[index]['icon'],
                                      Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Text(
                                          data[index]['title'],
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontFamily: "Nunito-bold"
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                      );

                    },
                  )),
              // Container(
              //           height: 80,
              //           width: 320,
              //           child: Row(
              //             crossAxisAlignment: CrossAxisAlignment.start,
              //             mainAxisAlignment: MainAxisAlignment.start,
              //             children: [
              //               Icon(
              //                 Icons.add_shopping_cart_outlined,
              //                 color: Colors.grey,
              //                 size: 28,
              //               ),
              //               Padding(
              //                 padding: const EdgeInsets.only(left: 10),
              //                 child: Text(
              //                   "Order History",
              //                   style: TextStyle(
              //                       fontSize: 18,
              //                       fontFamily: "Nunito-bold"
              //                   ),
              //                 ),
              //               )
              //             ],
              //           ),
              //         )
            ],
          ),
        ),
      ),
      );
  }
}
