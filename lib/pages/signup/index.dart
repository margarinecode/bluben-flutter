import 'package:bluben/pages/constants.dart';
import 'package:bluben/pages/home_app/index.dart';
import 'package:bluben/service/loginService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  bool passwordVisible = false;
  bool passwordConfirmVisible = false;
  TextEditingController email = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController password = TextEditingController();
  bool isChecked = false;
  String err = '';
  void togglePassword() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.fromLTRB(24, 60, 24, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      'Bluben',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        fontFamily: 'Nunito-bold',
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: SvgPicture.asset('assets/signup.svg', height: 300,),
                  )
                ],
              ),
              Center(
                child: Text(
                  err,
                  style: TextStyle(
                      color: Colors.red,
                      fontFamily: 'Nunito'
                  ),
                ),
              ),
              Form(
                  child: Column(
                      children: [ Container(
                        margin: EdgeInsets.only(top: 15),
                        decoration: BoxDecoration(
                            color: Colors.white10,
                            borderRadius: BorderRadius.circular(14)
                        ),
                        height: 45,
                        child: TextFormField(
                          controller: name,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.name,
                          cursorColor: PrimaryColor,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16),
                                  borderSide:
                                  BorderSide(color: PrimaryColor)),
                              contentPadding: EdgeInsets.all(12),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16),
                              ),
                              hintText: 'Name',
                              hintStyle: TextStyle(
                                fontFamily: "Nunito",
                              )
                          ),
                        ),
                      ),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius: BorderRadius.circular(14)
                          ),
                          height: 45,
                          child: TextFormField(
                            controller: email,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                            cursorColor: PrimaryColor,
                            decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16),
                                    borderSide:
                                    BorderSide(color: PrimaryColor)),
                                contentPadding: EdgeInsets.all(12),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                hintText: 'Email',
                                hintStyle: TextStyle(
                                  fontFamily: "Nunito",
                                )
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15, bottom: 10),
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius: BorderRadius.circular(14)
                          ),
                          height: 45,
                          child: TextFormField(
                            controller: password,
                            obscureText: !passwordVisible,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16),
                                  borderSide:
                                  BorderSide(color: PrimaryColor)),
                              contentPadding: EdgeInsets.all(12),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16),
                              ),
                              hintText: 'Password',
                              hintStyle: TextStyle(
                                fontFamily: "Nunito",
                              ),
                              suffixIcon: IconButton(
                                color: PrimaryColor,
                                splashRadius: 1,
                                icon: Icon(passwordVisible ? Icons
                                    .visibility_outlined : Icons
                                    .visibility_off_outlined),
                                onPressed: togglePassword,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 5),
                          child: Center(
                              child: Center(
                                  child: TextButton(
                                      style: ButtonStyle(
                                        minimumSize: MaterialStateProperty.all<Size>(Size(400, 50)),
                                        foregroundColor: MaterialStateProperty.all<Color>(PrimaryColor),
                                        overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                              (Set<MaterialState> states) {
                                            if (states.contains(MaterialState.hovered))
                                              return PrimaryColor.withOpacity(0.04);
                                            if (states.contains(MaterialState.focused) ||
                                                states.contains(MaterialState.pressed))
                                              return PrimaryColor.withOpacity(0.12);
                                            return null; // Defer to the widget's default.
                                          },
                                        ),
                                      ),
                                      onPressed:  ()async {
                                        if (email.text == null ||email.text == '') {
                                          setState(() {
                                            err = "Please Input Your Data";
                                          });
                                        }else {
                                          var log =await loginService().signup(name.text, email.text, password.text);
                                          if(await log != "Successfully add data!"){
                                            setState(() {
                                              err = log;
                                            });
                                          }else{
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) {
                                                  return HomePage();
                                                },
                                              ),
                                            );
                                          }
                                        }
                                      },
                                      child: Text(
                                        'Signup',
                                        style: TextStyle(
                                            fontFamily: 'Nunito',
                                            fontSize: 20
                                        ),
                                      )
                                  )
                              )
                          ),
                        )
                      ]
                  )
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'already have an account? ',
                      style: TextStyle(
                          color: Colors.grey[500]
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return SignUp();
                            },
                          ),
                        );
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(
                            color: PrimaryColor
                        ),
                      ),
                    )
                  ]
              ),
            ],
          ),
        )
    );
  }
}
