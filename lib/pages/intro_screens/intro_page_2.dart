// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class IntroPage2 extends StatelessWidget {
  const IntroPage2({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 243, 243, 243),
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 80, 25, 0),
          child: Column(
            children: [
              SvgPicture.asset(
                'assets/explore.svg',
                width: 220,
                alignment: Alignment.centerRight,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: Text(
                  'Exploring the fashion trends',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 45,
                    fontFamily: 'Nunito-bold',
                    height: 1,
                  ),
                ),
              ),
              Text(
                'We form an assortment that follows fashion trends.',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.grey[500],
                  fontSize: 20,
                  fontFamily: 'Nunito',
                  height: 1.2,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
