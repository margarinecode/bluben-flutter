// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IntroPage1 extends StatelessWidget {
  const IntroPage1({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 243, 243, 243),
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 80, 25, 0),
          child: Column(
            children: [
              SvgPicture.asset(
                'assets/boring.svg',
                width: 220,
                height: 350,
                fit: BoxFit.scaleDown,
                alignment: Alignment.centerRight,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: Text(
                  'No more boring things',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    height: 1,
                    fontWeight: FontWeight.w700,
                    fontSize: 45,
                    fontFamily: 'Nunito-bold',
                  ),
                ),
              ),
              Text(
                'Picking up accessories from popular European brands',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.grey[500],
                  fontSize: 20,
                  fontFamily: 'Nunito',
                  height: 1.2,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
