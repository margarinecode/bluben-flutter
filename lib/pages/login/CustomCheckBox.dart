import 'package:bluben/pages/constants.dart';
import 'package:flutter/material.dart';

class CustomCheckbox extends StatefulWidget {
  const CustomCheckbox({ Key? key }) : super(key: key);

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isChecked = !isChecked;
        });
      },
      child: Container(
        margin: EdgeInsets.only(right: 5),
        decoration: BoxDecoration(
            color: isChecked ? PrimaryColor : Colors.transparent,
            borderRadius: BorderRadius.circular(4),
            border: isChecked ? null : Border.all(color: Colors.grey, width: 1.5)
        ),
        width: 20,
        height: 20,
        child: isChecked ? Icon(Icons.check, size: 20,color: Colors.white,) : null,
      ),
    );
  }
}