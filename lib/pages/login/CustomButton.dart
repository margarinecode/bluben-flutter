import 'package:bluben/pages/home_app/index.dart';
import 'package:bluben/service/loginService.dart';
import 'package:flutter/material.dart';

class CustomPrimaryButton extends StatelessWidget {

  final Color buttonColor;
  final String textValue;
  final Color textColor;
  final String email;

  const CustomPrimaryButton({ this.buttonColor=Colors.black, this.textValue='', this.textColor=Colors.black, this.email=''});

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(14),
      elevation: 0,
      child: Container(
        height: 56,
        decoration: BoxDecoration(
            color: buttonColor,
            borderRadius: BorderRadius.circular(14)
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              // showDialog(
              //   context: context,
              //   builder: (context) {
              //     return AlertDialog(
              //       // Retrieve the text that the user has entered by using the
              //       // TextEditingController.
              //       content: Text(email),
              //     );
              //   },
              // );
              // loginService().login("salwa@gmail.com", "salwa123");
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) {
              //       return HomePage();
              //     },
              //   ),
              // );
            },
            borderRadius: BorderRadius.circular(14),
            child: Center(
              child: Text(
                textValue,
                style: TextStyle(
                  fontFamily: "Nunito",
                  color: textColor,
                  fontSize: 22,
                ),
              ),
            ),
          ),
        ),
      ),

    );
  }
}