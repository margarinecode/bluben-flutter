import 'package:bluben/pages/constants.dart';
import 'package:bluben/pages/home_app/index.dart';
import 'package:bluben/pages/login/CustomButton.dart';
import 'package:bluben/pages/login/CustomCheckBox.dart';
import 'package:bluben/pages/signup/index.dart';
import 'package:bluben/service/loginService.dart';
import 'package:bluben/store/loginStore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_it/get_it.dart';


class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}
class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  bool passwordVisible = false;
  bool passwordConfirmVisible = false;
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool isChecked = false;
  String err = '';
  void togglePassword() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
          body: Padding(
            padding: EdgeInsets.fromLTRB(24, 60, 24, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        'Bluben',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          fontFamily: 'Nunito-bold',
                        ),
                      ),
                    ),
                    SvgPicture.asset('assets/login.svg', height: 300,)
                  ],
                ),
                Center(
                  child: Text(
                      err,
                    style: TextStyle(
                        color: Colors.red,
                      fontFamily: 'Nunito'
                    ),
                  ),
                ),
                Form(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius: BorderRadius.circular(14)
                          ),
                          height: 45,
                          child: TextFormField(
                            controller: email,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                            cursorColor: PrimaryColor,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16),
                                  borderSide:
                                  BorderSide(color: PrimaryColor)),
                              contentPadding: EdgeInsets.all(12),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                hintText: 'Email',
                                hintStyle: TextStyle(
                                  fontFamily: "Nunito",
                                )
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15, bottom: 10),
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius: BorderRadius.circular(14)
                          ),
                          height: 45,
                          child: TextFormField(
                            controller: password,
                            obscureText: !passwordVisible,
                            decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(16),
                                        borderSide:
                                        BorderSide(color: PrimaryColor)),
                                    contentPadding: EdgeInsets.all(12),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                hintText: 'Password',
                                hintStyle: TextStyle(
                                  fontFamily: "Nunito",
                                ),
                                suffixIcon: IconButton(
                                  color: PrimaryColor,
                                  splashRadius: 1,
                                  icon: Icon(passwordVisible ? Icons
                                      .visibility_outlined : Icons
                                      .visibility_off_outlined),
                                  onPressed: togglePassword,
                                ),
                            ),
                          ),
                        ),
                        // Row(
                        //  mainAxisAlignment: MainAxisAlignment.start,
                        //  children: [
                        //      GestureDetector(
                        //       onTap: () {
                        //       setState(() {
                        //       isChecked = !isChecked;
                        //       });
                        //       },
                        //         child: Container(
                        //           margin: EdgeInsets.only(right: 5),
                        //           decoration: BoxDecoration(
                        //               color: isChecked ? PrimaryColor : Colors.transparent,
                        //               borderRadius: BorderRadius.circular(4),
                        //               border: isChecked ? null : Border.all(color: Colors.grey, width: 1.5)
                        //          ),
                        //           width: 20,
                        //           height: 20,
                        //           child: isChecked ? Icon(Icons.check, size: 20,color: Colors.white,) : null,
                        //       ),
                        //     ),
                        //    Column(
                        //     crossAxisAlignment: CrossAxisAlignment.start,
                        //     children: [
                        //       Text(
                        //     'By creating an account, you agree to our',
                        //       style: TextStyle(
                        //       fontFamily: "Nunito",
                        //       ),
                        //     ),
                        //       Text(
                        //       'Term & Condition',
                        //         style: TextStyle(
                        //         fontFamily: "Nunito",
                        //      ),
                        //     )
                        //    ],
                        //   )
                        //   ],
                        // ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 25),
                         child: Center(
                            child: Center(
                              child: TextButton(
                                  style: ButtonStyle(
                                  minimumSize: MaterialStateProperty.all<Size>(Size(400, 50)),
                                  foregroundColor: MaterialStateProperty.all<Color>(PrimaryColor),
                                  overlayColor: MaterialStateProperty.resolveWith<Color?>(
                                        (Set<MaterialState> states) {
                                      if (states.contains(MaterialState.hovered))
                                        return PrimaryColor.withOpacity(0.04);
                                      if (states.contains(MaterialState.focused) ||
                                          states.contains(MaterialState.pressed))
                                        return PrimaryColor.withOpacity(0.12);
                                      return null; // Defer to the widget's default.
                                    },
                                  ),
                                  ),
                              onPressed:  ()async {
                                if (email.text == '' || password.text == '') {
                                  setState(() {
                                    err = "Please Input Your Email and Password";
                                  });
                                }else {
                                  var log =await loginService().login(email.text, password.text);
                                  if(await log != "success"){
                                    setState(() {
                                      err = log;
                                    });
                                  }else{
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) {
                                          return HomePage();
                                        },
                                      ),
                                    );
                                  }
                                }
                              },
                                child: Text(
                                  'Login',
                                  style: TextStyle(
                                  fontFamily: 'Nunito',
                                  fontSize: 20
                                ),
                                )
                              )
                            )
                           ),
                      )
                      ]
                    )
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'not have an account? ',
                        style: TextStyle(
                            color: Colors.grey[500]
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return SignUp();
                              },
                            ),
                          );
                        },
                        child: Text(
                          'Signup',
                          style: TextStyle(
                            color: PrimaryColor
                          ),
                        ),
                      )
                    ]
                ),
              ],
            ),
          )
    );
  }
}
